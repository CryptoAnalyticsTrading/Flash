'use strict';

// Application infra
const System = require('./bin/system');

// Config
const AppConfig = require('config.json')('./config/application.json');   

// Logging & environment
const logger = require('./bin/log4js')
logger.level = AppConfig.env.log_level

const { table } = require('table');
const jsonToTable = require('json-to-table');


// App flow
const TaskManager = require('./tasks/taskManager');
 
(async function(){

	logger.info(`*********`)
	logger.info(`* Flash *`)
	logger.info(`*********`)
	logger.info(`App started on port: ${AppConfig.env.port}`)

	// Run system startup tasks handler
	System.startup();

	// Run microservice tasks
	TaskManager.listenStdTickersFromExchange(`event.read.rabbitmq`, AppConfig.tasks.unbBinArb, AppConfig.algorithms.unbBinArb)	
		
	// List all tasks
	logger.info(`Service tasks status:`)
	logger.info(`\n${table(jsonToTable(TaskManager.getActiveTasks()))}`)

})();