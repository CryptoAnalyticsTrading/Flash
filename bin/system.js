'use strict'; 

/************************************************************
 * Imports
 ***********************************************************/
const Util = require('util');
const Memwatch = require('memwatch-next');
const logger = require('log4js').getLogger();

/************************************************************
 * Public functions
 ***********************************************************/

module.exports = {

  /**
   * Runs all application startup functions
   * 
   * @param
   * 
   * @returns
   */
  startup() {
    watchMemoryLeaks()
  }

}

/************************************************************
 * Private functions
 ***********************************************************/

function watchMemoryLeaks() {
  let hd = null;
  Memwatch.on('leak', (info) => {
    logger.info('memwatch::leak');
    logger.error(info);
    if (!hd) 
      hd = new Memwatch.HeapDiff();
    else {
      const diff = hd.end();
      logger.error(Util.inspect(diff, true, null));
      hd = null;
    }
  });
}