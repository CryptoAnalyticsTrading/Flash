'use strict'; 

/************************************************************
 * Public functions
 ***********************************************************/

const rmNullInArray = array => array.filter( v => v != null )

const nullOrEmptyArray = array => array == null || array.length < 1 

module.exports.rmNullInArray = rmNullInArray;
module.exports.nullOrEmptyArray = nullOrEmptyArray;