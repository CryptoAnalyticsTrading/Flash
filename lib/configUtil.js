'use strict';

/************************************************************
 * Imports
 ***********************************************************/

 /************************************************************
 * Public functions
 ************************************************************/

const toExchangeNamesArray = (supportedExchanges) => {
  const exchanges = []

  for (let exchange of supportedExchanges)
    exchanges.push(exchange.name)

  return exchanges
}

 /************************************************************
 * Private functions
 ************************************************************/

 /************************************************************
 * Exports
 ************************************************************/
module.exports.toExchangeNamesArray = toExchangeNamesArray;
