/**
 * 
 * Service constant variables
 * 
 * @type {string}
 */
module.exports.CURRENCY_TRADE_STATUS_UNKNOWN = `UNK`;

module.exports.CURRENCY_PROP_ACTIVE = `Active`;