'use strict'; 

const isSanitizedNumber = number => !Number.isNaN(number) && Number.isFinite(number)

const inRange = (value, minValue, maxValue) => value >= minValue && value <= maxValue 

/**
 * 
 * @param {number} timestamp_s UTC timestamp 
 * @param {number} timestampLife_s How long the timestamp is valid for in Seconds 
 */
const isRecent = (timestamp_s, timestampLife_s) => {
  let timestampNow_s = Math.floor(new Date().getTime() / 1000)

  return timestampNow_s - timestamp_s <= timestampLife_s
} 

const isWhiteListed = (value, whitelist) => whitelist.includes(value) 

/************************************************************
 * Exports
 ***********************************************************/

module.exports.inRange = inRange
module.exports.isRecent = isRecent
module.exports.isWhiteListed = isWhiteListed
module.exports.isSanitizedNumber = isSanitizedNumber 