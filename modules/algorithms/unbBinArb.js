'use strict'; 

const dataUtil = require('../../lib/dataUtil')

function UnbBinArb() {
    this._name = "UnbBinArb"
}

// Todo: Fix terrible implementation later to be better than O(n^2)
// Unbalanced Binary Arbitrage
UnbBinArb.prototype.run = function(dataSet, config) {

    // 1. Passthrough the data set
    // 2. Categorize each ticker pair into its own map, creating a collection of tickers under ticker pair
    // 3. Iterate though each ticker pair
    // 4. Find arbitrage opportunities in each collection (if any)
    // 5. Add arbitrage opportunities to resulting list of spreads
	const tickerPairMap = {}
	const profitableArbitrageSpreads = []
  const minViableProfit = config.minViableProfit
  const percRounding = config.percRounding
  const rawRounding = config.rawRounding

	// Categorize spreads by BASE/QUOTE pairs
	const dataSetLen = dataSet.length
    for (let i = 0; i < dataSetLen; i += 1) {
      const mapPair = dataSet[i].pair

      if (typeof tickerPairMap[mapPair] === "undefined")
        tickerPairMap[mapPair] = []
			
      tickerPairMap[mapPair].push(dataSet[i])
    }

	// Find profitable spreads in each BASE/QUOTE pair collection	
	for (const [, tickerDataSetArray] of Object.entries(tickerPairMap)) {

		const dataLen = tickerDataSetArray.length
		for (let i = 0; i < dataLen - 1; i += 1) {
			for (let j = i + 1; j < dataLen; j += 1) {

				if (isProfitable(tickerDataSetArray[i], tickerDataSetArray[j], minViableProfit)) {

          const [buy, sell] = getBuyAndSellTickerDataSet(tickerDataSetArray[i], tickerDataSetArray[j])
          const percGain = calculatePercProfit(buy, sell).toFixed(percRounding)
          const rawGain = calculateRawProfit(buy, sell).toFixed(rawRounding)
					profitableArbitrageSpreads.push({ 
            sell, 
            profit: {
              rawGain,
              percGain 
            }, 
            buy 
					})
				}

			}
		}	

	}

	return profitableArbitrageSpreads
}

// Note: At this moment, trading fees are not inlcuded in profit calculation
// Todo: Add exchange fee in price estimation
function isProfitable(tickerDataSetA, tickerDataSetB, minViableProfit) {

	// eslint-disable-next-line
	const [buyTicker, sellTicker] = getBuyAndSellTickerDataSet(tickerDataSetA, tickerDataSetB)  
  const profit = calculatePercProfit(buyTicker, sellTicker)  
  if (!dataUtil.isSanitizedNumber(profit))
    return false  

	return profit >= minViableProfit
}

const calculatePercProfit = (buyTicker, sellTicker) => (sellTicker.bid - buyTicker.ask) / buyTicker.ask * 100
const calculateRawProfit = (buyTicker, sellTicker) => sellTicker.bid - buyTicker.ask
//eslint-disable-next-line
const getBuyAndSellTickerDataSet = (tickerDataSetA, tickerDataSetB) => tickerDataSetA.ask <= tickerDataSetB.ask ? [tickerDataSetA, tickerDataSetB] : [tickerDataSetB, tickerDataSetA]


module.exports = new UnbBinArb()