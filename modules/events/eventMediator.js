'use strict'; 

/************************************************************
 * Imports
 ***********************************************************/

const logger = require('log4js').getLogger();

// Import emitters throughout the application
const rabbitMqEventEmitter = require('../queue/rabbitMqBroker').rabbitMqEventEmitter
const rabbitMqBroker = require('../queue/rabbitMqBroker').rabbitMqBroker

// Import analyzers
const unbBinArb = require('../algorithms/unbBinArb')

/************************************************************
 * Public 
 ***********************************************************/

function EventMediator() {
  this._name = "EventMediator"
} 

// To-do Refactor to be generic event handeller
EventMediator.prototype.handleUnbBinArb = function(eventToListen, algConfig, outboundDataFlowConfig) {
  rabbitMqEventEmitter.on(eventToListen, async (message) => {
    const dataSet = JSON.parse(message)
    const unifiedDataSet = []
    const dataSetLen = dataSet.length
    for (let i = 0; i < dataSetLen; i += 1) 
      unifiedDataSet.push(...dataSet[i].tickers)

    logger.info(`Recieved ${unifiedDataSet.length} tickers from data harvester module`)
    const profitableUnbBinArbSpreads = unbBinArb.run(unifiedDataSet, algConfig)
    logger.info(`Found profitable ${profitableUnbBinArbSpreads.length} spreads`)

    const rabbitMqSettings = {
      exchangeType: outboundDataFlowConfig.mqSettings.exchangeType, 
      exchangeName: outboundDataFlowConfig.mqSettings.exchangeName
    }
    const options = outboundDataFlowConfig.mqSettings.options

    const rabbitMqChannel = await rabbitMqBroker.openConnectionChannel(outboundDataFlowConfig.url)

    rabbitMqBroker.publishMsgOnExchange(rabbitMqChannel, rabbitMqSettings, JSON.stringify(profitableUnbBinArbSpreads), options)
  });
}

/************************************************************
 * Exports 
 ***********************************************************/
module.exports = new EventMediator()