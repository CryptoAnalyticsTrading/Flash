'use strict';

/************************************************************
 * Module dependancies
 ***********************************************************/
const rabbitMqBroker = require('../queue/rabbitMqBroker').rabbitMqBroker

/************************************************************
 * Public functions
 ***********************************************************/
function StdTickersListener() {
  this._name = "StdTickersListener"

  this.lastSubResult = null
}

StdTickersListener.prototype.listenToQueue = async function(eventEmitterName, config) {
  const mqUrl = config.mqSettings.mqUrl

  const channel = await rabbitMqBroker.openConnectionChannel(mqUrl)
  rabbitMqBroker.listenExchangeEmitter(channel, config.mqSettings, eventEmitterName)
}

Object.seal(StdTickersListener)

/************************************************************
 * Exports
 ***********************************************************/
module.exports = new StdTickersListener()