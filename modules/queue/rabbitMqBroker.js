/***************************************
* Library encapsulates work with 
* rabbit mq message broker
 ***************************************/
'use strict';

/************************************************************
 * Module dependancies
 ***********************************************************/

// RabbitMq library
const amqp = require('amqplib')

const logger = require('log4js').getLogger();

const EventEmitter = require('events')
class RabbitMqEventEmitter extends EventEmitter {}
const rabbitMqEventEmitter = new RabbitMqEventEmitter()

/************************************************************
 * Public functions
 ***********************************************************/

function RabbitMqBroker() {
  this._name = "RabbitMqBroker"
  
  this._connection = null

  this._DEF_MQ_ASK = {
    noAck: true 
  }
  this._DEF_PATTERN = ''
}
    
RabbitMqBroker.prototype.openConnectionChannel = async function(endpoint) {
  const FUNC_NAME = "openConnectionChannel"
  let channel = null

  try {

    if (this._connection == null) 
      this._connection = await amqp.connect(endpoint)

    channel = await this._connection.createChannel()  

  } catch (err) {
    logger.error(`[${this._name}:${FUNC_NAME}] Unexpected exception raised by RabbitMq connector ${err}`)
    throw Error("Unable to establish connection with RabbitMq")
  }

  return channel
}

// Listen to exchange
RabbitMqBroker.prototype.listenExchangeEmitter = async function(channel, mqSourceInfo, eventName) {
  const FUNC_NAME = "listenExchangeEmitter"

  try {

    await channel.assertExchange(mqSourceInfo.exchangeName, mqSourceInfo.exchangeType, mqSourceInfo.options);
    
    let assertedQueue = await channel.assertQueue(mqSourceInfo.bindQueueName, mqSourceInfo.options)

    await channel.bindQueue(assertedQueue.queue, mqSourceInfo.exchangeName, this._DEF_PATTERN)

    await channel.consume(assertedQueue.queue, function(mqMessage) {
      const mqMessageStr = mqMessage.content.toString()
      logger.info(`\n${'-'.repeat(10)}`)
      logger.info(` [x] Recieved message of length ${mqMessageStr.length}`)
      rabbitMqEventEmitter.emit(eventName, mqMessageStr)
    }, this._DEF_MQ_ASK);

  } catch (err) {
    logger.error(`[${this._name}:${FUNC_NAME}] Unexpected exception raised by RabbitMq ${err}`)
    throw Error(`Unable to listen to RabbitMq exchange ${err.message}`)
  }

}

// eslint-disable-next-line
RabbitMqBroker.prototype.publishMsgOnExchange = async function(channel, mqSourceInfo, msg, options = {
  durable: true
}) {
  const FUNC_NAME = "publishMsgOnExchange"
  let messagePublished = false 

  try {

    if (channel == null)
      throw Error("Channel has not been initialized")

    await channel.assertExchange(mqSourceInfo.exchangeName, mqSourceInfo.exchangeType, options)
    const noRoute = ''
    // eslint-disable-next-line
    messagePublished = await channel.publish(mqSourceInfo.exchangeName, noRoute, Buffer.from(msg))
    logger.info(` (X) Message published on exchange`);

  } catch (err) {
    logger.error(`[${this._name}:${FUNC_NAME}] Exception raised sending message to exchange ${err}`)
    throw Error(`Unable to send message to ${mqSourceInfo.exchangeName}`)
  }

  return messagePublished
}

RabbitMqBroker.prototype.closeConnection = async function() {
  const FUNC_NAME = "closeConnection" 
  let closePromise = null

  try {

    if (this._connection == null) 
      throw Error(`Connection is already null`)

    closePromise = await this._connection.close()

  } catch (err) {
    logger.error(`[${this._name}:${FUNC_NAME}] Unexpected exception raised by RabbitMq ${err}`)
    throw Error("Unable to close connection channel")
  }

  return closePromise
}

/************************************************************
 * Exports
 ***********************************************************/

// REFACTOR TO SINGLETON 
module.exports.rabbitMqBroker = new RabbitMqBroker()
module.exports.rabbitMqEventEmitter = rabbitMqEventEmitter 