'use strict';

/************************************************************
 * Module dependancies
 ***********************************************************/

// Listeners
const stdTickersListener = require('../modules/listeners/stdTickersListener')
const eventMediator = require('../modules/events/eventMediator')

/************************************************************
 * Public functions 
 ***********************************************************/

function TaskManager() {
  this._name = "TaskManager"
  this._activeTasks = []
}

TaskManager.prototype.listenStdTickersFromExchange = function(eventEmitterName, taskConfig, analyzerConfig) {

  stdTickersListener.listenToQueue(eventEmitterName, taskConfig.listenStdTickers)
  eventMediator.handleUnbBinArb(eventEmitterName, analyzerConfig, taskConfig.publishAnalyzedTickers)

  this._activeTasks.push({
    task: `listenStdTickersFromExchange`,
    status: `active`
  })
}

TaskManager.prototype.getActiveTasks = function() {
  return this._activeTasks
}

/************************************************************
 * Private functions 
 ***********************************************************/

/************************************************************
 * Exports 
 ***********************************************************/
module.exports = new TaskManager()
