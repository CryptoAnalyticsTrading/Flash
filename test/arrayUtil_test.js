'use strict'; 

const expect = require('chai').expect
const nullOrEmptyArray = require('../lib/arrayUtil').nullOrEmptyArray
const rmNullInArray = require('../lib/arrayUtil').rmNullInArray

describe('arrayUtil', () => {
  
  it('Returns nullOrEmptyArray(null) -> true', () => {

    // Arrange
    const someArray = null

    // Act 
    const result = nullOrEmptyArray(someArray)

    // Assert
    expect(result).to.be.equal(true)

  });

  it('Returns nullOrEmptyArray([]) -> true', () => {

    // Arrange
    const someArray = []

    // Act 
    const result = nullOrEmptyArray(someArray)

    // Assert
    expect(result).to.be.equal(true)

  });

  it('Returns nullOrEmptyArray([1,2,3]) -> False', () => {

    // Arrange
    const someArray = [1,2,3]

    // Act 
    const result = nullOrEmptyArray(someArray)

    // Assert
    expect(result).to.be.equal(false)

  });

  it('Returns rmNullInArray([1,null,3,null,5,6]) -> [1,3,5,6]', () => {

    // Arrange
    const someArray = [1,null,3,null,5,6]
    const cleanArray = [1,3,5,6]

    // Act 
    const result = rmNullInArray(someArray)

    // Assert
    expect(result).to.have.members(cleanArray)

  });

  it('Returns rmInNullArray([1,3,5,6]) -> [1,3,5,6]', () => {

    // Arrange
    const someArray = [1,3,5,6]
    const cleanArray = [1,3,5,6]

    // Act 
    const result = rmNullInArray(someArray)

    // Assert
    expect(result).to.have.members(cleanArray)

  });

});