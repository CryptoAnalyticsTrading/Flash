'use strict';

const expect = require('chai').expect
const toExchangeNamesArray = require('../lib/configUtil').toExchangeNamesArray

describe('configUtil', () => {
  it('Returns toExchangeNamesArray(supportedExchangesConfig) -> ["kraken", "cex", "bitfinex"]', function() {

    // Arrange
    const testSupportedExch = [ 
      { "name": "kraken" }, 
      { "name": "cex" },
      { "name": "bitfinex" }
    ]
    const testSupportedExchArray = ["kraken", "cex", "bitfinex"]

    // Act 
    const actualSupportedExch = toExchangeNamesArray(testSupportedExch)

    // Assert
    expect(actualSupportedExch).to.contain.members(testSupportedExchArray)
  });
});