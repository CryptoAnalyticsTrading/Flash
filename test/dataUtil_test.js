'use strict';

const expect = require('chai').expect
const Filters = require('../lib/dataUtil')

describe('dataUtil', () => {

  it('Returns inRange(5, 2, 10) => True', () => {

    // Arrange 
    const bidAsk = 5
    const minBidAsk = 2
    const maxBidAsk = 10

    // Act
    const isInRange = Filters.inRange(bidAsk, minBidAsk, maxBidAsk) 

    // Assert
    expect(isInRange).to.be.equal(true)

  })

  it('Returns inRange(0.3, 0.1, 0.5) => True', () => {

    // Arrange 
    const bidAsk = 0.3 
    const minBidAsk = 0.1 
    const maxBidAsk = 0.5 

    // Act
    const isInRange = Filters.inRange(bidAsk, minBidAsk, maxBidAsk) 

    // Assert
    expect(isInRange).to.be.equal(true)

  })

  it('Returns inRange(0.1, 0.1, 0.5) => True', () => {

    // Arrange 
    const bidAsk = 0.1 
    const minBidAsk = 0.1 
    const maxBidAsk = 0.5 

    // Act
    const isInRange = Filters.inRange(bidAsk, minBidAsk, maxBidAsk) 

    // Assert
    expect(isInRange).to.be.equal(true)

  })

  it('Returns inRange(0.09, 0.1, 0.5) => False', () => {

    // Arrange 
    const bidAsk = 0.09 
    const minBidAsk = 0.1 
    const maxBidAsk = 0.5 

    // Act
    const isInRange = Filters.inRange(bidAsk, minBidAsk, maxBidAsk) 

    // Assert
    expect(isInRange).to.be.equal(false)

  })

  it('Returns isRecent(1537224038, 600) => False', () => {

    // Arrange
    const tickerTimestamp_s = 1537224038
    const validFor_s = 600

    // Act 
    const recent = Filters.isRecent(tickerTimestamp_s, validFor_s)

    // Assert
    expect(recent).to.be.equal(false)

  })

  it('Returns isRecent(now - 3, 600) => True', () => {

    // Arrange
    const tickerTimestamp_s = Math.floor(new Date().getTime() / 1000) - 3
    const validFor_s = 600

    // Act 
    const recent = Filters.isRecent(tickerTimestamp_s, validFor_s)

    // Assert
    expect(recent).to.be.equal(true)

  })

  it('Returns isWhiteListed(false, [true, false]) => True', () => {
    
    // Arrange
    const checkWhitelistVal = false
    const whitelist = [true, false]

    // Act 
    const whiteListed = Filters.isWhiteListed(checkWhitelistVal, whitelist) 

    // Assert
    expect(whiteListed).to.be.equal(true)

  })
  
  it('Returns isWhiteListed(false, [true]) => False', () => {
    
    // Arrange
    const checkWhitelistVal = false
    const whitelist = [true]

    // Act 
    const whiteListed = Filters.isWhiteListed(checkWhitelistVal, whitelist) 

    // Assert
    expect(whiteListed).to.be.equal(false)

  })

  it('Returns isWhiteListed(false, []) => False', () => {
    
    // Arrange
    const checkWhitelistVal = false
    const whitelist = [true]

    // Act 
    const whiteListed = Filters.isWhiteListed(checkWhitelistVal, whitelist) 

    // Assert
    expect(whiteListed).to.be.equal(false)

  })
  
})