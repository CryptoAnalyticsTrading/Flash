// 'use strict';

// const chai = require('chai')
// chai.should();
// const expect = chai.expect

// const sinon = require('sinon')
// const sinonChai = require("sinon-chai");
// chai.use(sinonChai);

// const rabbitMqBroker = require('../modules/queue/rabbitMqBroker').rabbitMqBroker
// const rabbitMqEventEmitter = require('../modules/queue/rabbitMqBroker').rabbitMqEventEmitter

// describe('rabbitMqBroker', () => {

// //   it('Catches openConnectionChannel("nowhere") -> Error thrown', async () => {

// //     // Arrange
// //     const connectionString = "nowhere"
// //     const expectedErrorMsg = 'Unable to establish connection with RabbitMq'
// //     let errorMsg = ''

// //     // Act
// //     try {
    
// //       await rabbitMqBroker.openConnectionChannel(connectionString)
      
// //     } catch (err) {
// //       errorMsg = err.message
// //     }

// //     // Assert
// //     expect(errorMsg).to.be.equal(expectedErrorMsg)    
// //   })

//   it('Reads listenExchangeEmitter(channel, mqSouceInfo, eventName)', async () => {
    
//     // Arrange 
//     const expectedSubMessage = `hello world from flash`
//     const mqSettings = {
//       vendor: "rabbitMq",
//       url: "amqp://localhost",  
//       options: {
//         durable: true 
//       },
//       exchangeType: "fanout",
//       exchangeName: "test.walle.raw_tickers.exchange", 
//       bindQueueName: "test.walle.raw_tickers.queue"
//     }
//     // const mqSettings = {
//     // "vendor": "rabbitMq",
//     //     "url": "amqp://localhost",  
//     //     "exchangeName": "walle.raw_tickers.exchange", 
//     //     "exchangeType": "fanout",
//     //     "bindQueueName": "walle.raw_tickers.queue",
//     //     "options": {
//     //       "durable": true
//     //     }
//     //   }

//     const testEventName = "test.event.read.rabbitmq"
//     const spy = sinon.spy()

//     // Act 

//     // Setup observable on event
//     rabbitMqEventEmitter.on(testEventName, spy)

//     // Setup listener to rabbit mq
//     const testChannel = await rabbitMqBroker.openConnectionChannel(mqSettings.url)
//     rabbitMqBroker.listenExchangeEmitter(testChannel, mqSettings, testEventName)

//     // Write to rabbit mq
//     await rabbitMqBroker.publishMsgOnExchange(testChannel, mqSettings, expectedSubMessage)

//     await rabbitMqBroker.closeConnection() 

//     console.log(`-----`)
//     console.log(`spy called ${spy.called}`)
//     console.log(`spy call count ${spy.callCount}`)
//     console.log(`spy get calls: ${spy.getCalls()}`)

//     // Assert
//     expect(spy).to.have.been.calledWith(expectedSubMessage)
//   })

// /*
//   it('Sequentilly (2) writes/reads rabbitmq flash.test.exchange', async () => {
    
//     // Arrange 
//     const rabbitMqBrokerPub = new RabbitMqBroker()
//     const pubMessages = [`hello 1`, `hello 2`, `hello 3`, `hello 4`]

//     const rabbitMqBrokerSub = new RabbitMqBroker()
//     let subMessages = [] 
//     let bindedQueue = null 

//     const exchangeName = 'flash.test.exchange'

//     // Act 
//     try {

//       // Bind subscriber first, before can recieve messages from publishers
//       await rabbitMqBrokerSub.openConnectionChannel(rabbitMqUrl)
//       bindedQueue = await rabbitMqBrokerSub.bindQueueToExchange(exchangeName, 'fanout')

//       await rabbitMqBrokerPub.openConnectionChannel(rabbitMqUrl)
//       for (let i = 0; i < pubMessages.length; i += 1)
//         rabbitMqBrokerPub.publishMsgOnFanout(exchangeName, pubMessages[i])
      
//       for (let i = 0; i < pubMessages.length; i += 1)
//         // eslint-disable-next-line
//         subMessages.push((await rabbitMqBrokerSub.readFromQueue(bindedQueue)).content.toString())

//     } catch (err) {
//       subMessages = err.message
//     } finally {
//       await rabbitMqBrokerPub.closeConnectionChannel()
//       await rabbitMqBrokerSub.closeConnectionChannel()
//     }

//     // Assert
//     expect(subMessages).to.eql(pubMessages)
//   })
// */
 
// })